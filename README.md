# Nonlinear Control Lab 1
One of the labs in Nonlinear Control 2023/2024 (instructed by Prof. Maria Prandini). This lab is mostly about feedback linearization and was done using MATLAB version R2023b.


# Included Contents
### Live Scripts:
- [main.mlx](main.mlx)

### Simulink Models:
- [robot_arm_fblin.slx](robot_arm_fblin.slx)
![alt text](figures/robot_arm_fblin.jpeg)

- [robot_arm_simple.slx](robot_arm_simple.slx)
![alt text](figures/robot_arm_simple.jpeg)

