function [x_bar, u_bar] = get_eq(theta_l, m, g, l, k)
%GET_EQ Summary of this function goes here
%   Detailed explanation goes here
    x_bar = [theta_l+m*g*l/k*cos(theta_l) ; theta_l ; zeros(1,length(theta_l)) ; zeros(1,length(theta_l))];
    u_bar = m*g*l*cos(theta_l);
end

